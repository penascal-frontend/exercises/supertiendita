var request = require('request-promise');

module.exports = async (req, res, next) => {

    const { url, method, body } = req
    const host = req.get('Host')

    if (url.startsWith('/orders') && method === 'POST') {
        const lines = body.lines
          .reduce((acum, elem) => ({...acum, [elem.id]: elem}), {})
        const line_ids = Object.keys(lines)
        const modifications = []
        
        const stocks = JSON.parse(await request.get(`http://${host}/store`))
          .filter(product => line_ids.includes(product.id))
          .reduce((acum, elem) => ({...acum, [elem.id]: elem}), {})
        
        for (let id of line_ids) {
            const line = lines?.[id] ?? null
            const stock = stocks?.[id] ?? null

            if (!line)
                res.status(406).json({
                    error: 'Not Acceptable',
                    message: 'The error reading line.'
                }).end()
            
            if (!stock)
                res.status(406).json({
                    error: 'Not Acceptable',
                    message: 'The error reading stock.'
                }).end()
                
            if (line.quantity > stock.stock)
                res.status(406).json({
                    error: 'Not Acceptable',
                    message: 'Requested quantity exceeds its stock.'
                }).end()
            
            stock.stock -= line.quantity
            modifications.push(stock)
        }

        for (let stock of modifications) {
            console.log(stock)
            await request.put(`http://${host}/store/${stock.id}`, {
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(stock)
            })
        }
    } 
    next()
}
