# Esta plantilla de definicion de Historias de Ususario, ha de responder a las siguientes preguntas:
#  1. Roles       ¿Quién es el rol que tiene una necesidad o requerimiento específico?
#  2. Necesidad   ¿Cuál es la necesidad que el usuario intenta cumplir?
#  3. Razon       ¿Por qué es importante esta necesidad para el usuario o para el éxito del proyecto?
#  4. Aceptacion  ¿Cuáles son los criterios de aceptación que indican cuándo la historia de usuario se considera completada?
#  5. Relaciones  ¿Cómo se relaciona esta historia de usuario con otras funcionalidades o requerimientos del sistema?

Roles:
  cliente

Necesidad:
  CUANDO el cliente quita un producto a la cesta 
  QUIERO que el cliente valide la eliminación
  QUIERO que desaparezca el producto de la cesta
  QUIERO que aparezca el producto en el selector de productos
  QUIERO que recalcule el importe total

Razon:
  PARA evitar que el cliente pueda eliminar un producto sin querer
  PARA mantener actualizado el total que va gastando
  PARA que el cliente pueda volver a seleccionar el producto

Aceptacion:

Relaciones:
