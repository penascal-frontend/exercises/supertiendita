<style>
img { 
    height: 600px;
}
</style>

# SuperTiendita!

## Propuesta de mejora UI/UX

### Objetivo
Agilizar la seleccion de cantidades a reservar, y que el cliente tarde menos en decidir.

### Cambios
- Eliminar el input de cantidad.
- Agregar dos botones para incrementar o decrementar la cantidad.
- Capturar el evento de la rueda del raton, para incrementar o decrementar la cantidad. 

Nota: Los incrementos irán en kilos enteros. Ya se encargará el empaquetador de cestas de selecfionar el peso más acercado a lo seleccionado.

## Diseño base
Así quedaría el diseño base.
![Mobile](./assets/mobile.png "mobile design"){ width: 200px; }

## Objetivos futuros
Así quedarían los diseños futuros para la funcionalidad en la que los clientes que puedan realizar sus pedidos desde casa. y luego pasar a recogerlos.

![Tablet](./assets/tablet.png "tablet design")
![Desktop](./assets/desktop.png "desktop design")

