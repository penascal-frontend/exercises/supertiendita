<style>
img { 
    height: 600px;
}
</style>

# SuperTiendita!

## Entorno
La SuperTiendita tiene un sistema automatizado de crear la cesta de la compra para el cliente.
Tienen PDAs que ejecutan esta aplicacion, en la cual definen lo que quieren comprar.
En la caja, finalizan el proceso de compra y realizan el pago.

## Objetivo
El objetivo de esta aplicación es dejar que cualquier usuario pueda confeccionarse su lista de la compra.

## Diseño base
Ese partirá del siguiente diseño.
![Mobile](./assets/mobile.png "mobile design"){ width: 200px; }

## Objetivos futuros
En un futuro, se quiere habilitar a los clientes que puedan realizar sus pedidos desde casa. y luego pasar a recogerlos.

en este caso se realizará un diseño responsivo tanto para tablet como para desktop usando estos diseños:
![Tablet](./assets/tablet.png "tablet design")
![Desktop](./assets/desktop.png "desktop design")

