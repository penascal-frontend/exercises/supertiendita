export class Store {
    #value
    #subscribers
    
    constructor (initialValue) {
        this.#value = initialValue
        this.#subscribers = []
    } 
    
    subscribe(callback) {
        this.#subscribers.push(callback)
    }
    
    unsubscribe(callback) {
        this.#subscribers = this.#subscribers.filter(fn => fn != callback)
    }
    
    set value(value) {
        this.#value = value
        this.#subscribers.forEach(fn => fn(this.#value))
    }

    get value() { return this.#value}

}

function createStore(initialValue) {
    return new Store(initialValue)
}
