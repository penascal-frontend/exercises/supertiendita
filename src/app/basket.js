import { basketStore } from '@/core/stores'
import { sortProductsByName } from '@/app/product'

function cleanBasket() {
    document.getElementById("basket").innerHTML = ""
}

function render(products) {
    const basket = document.getElementById('basket')
    basket.innerHTML = ''

    products
      .sort(sortProductsByName)
      .forEach(product => {
        const article = document.createElement('article')
        article.id = product.id
        article.innerHTML = `
          <div>
            <span>${product.name}</span>
            <span>
                ${product.price.toFixed(2)}€/Kg
                x ${product.quantity}Kg =
                ${(+product.quantity * product.price).toFixed(2)}€
            </span>
          </div>
          <button type="button">+</button>
          <button type="button">-</button>
        `

        function updateQuantity(quantity) {
            product.quantity = Math.max(Math.min(product.quantity + quantity, product.stock), 0)
            basketStore.value = [
                ...basketStore.value.filter(p => p.id != product.id),
                product
            ]
        }

        const buttons = article.querySelectorAll('button')
        buttons[0].addEventListener('click', event => updateQuantity(1))
        buttons[1].addEventListener('click', event => updateQuantity(-1))

        article.addEventListener('wheel', event => {
            event.preventDefault()
            const step = event.deltaY < 0 ? 1 : -1;
            updateQuantity(step)
        });
        basket.appendChild(article)
    })

}

basketStore.subscribe(render)

export { cleanBasket }
