import config from '@/core/config'
import { productStore, basketStore, isAddingToBasketStore } from '@/core/stores'
import { sortProductsByName } from '@/app/product'


async function loadProducts() {
  const selector = document.getElementById('selector')
  selector.innerHTML = '<option>Cargando..</option>'
  
  const response = await fetch(config.productURL)
  productStore.value = await response.json()
}

async function addSelectedToBasket() {
  if (isAddingToBasketStore.value) return 
  isAddingToBasketStore.value = true

  const selector = document.getElementById('selector')
  const id = selector.value
  if (!id) return
  if (basketStore.value.find(p => p.id === id)) return 

  const response = await fetch(`${config.productURL}/${id}`)
  const product = await response.json()

  productStore.value = productStore.value.filter(p => p.id != id)

  basketStore.value = [
    ...basketStore.value,
    {...product, quantity: 0}
  ]

  isAddingToBasketStore.value = false
}


function render(products) {
  const options = products
    .sort(sortProductsByName)
    .map(product =>
      `<option value="${product.id}">${product.name} - ${product.price.toFixed(2)}€/Kg</option>`
    )
    .join('')

  document.getElementById('selector').innerHTML = options
}

productStore.subscribe(render)

export { loadProducts, addSelectedToBasket }

