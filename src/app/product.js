const sortProductsByName = (a, b) => a.name.localeCompare(b.name)

export { sortProductsByName }
