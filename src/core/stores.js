import { Store } from '@/lib/store'

const productStore = new Store([])
const basketStore = new Store([])
const isAddingToBasketStore = new Store(false)

export { productStore, basketStore, isAddingToBasketStore }
