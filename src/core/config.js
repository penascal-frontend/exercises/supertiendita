export default {
    productURL: import.meta.env.VITE_PRODUCT_URL,
    orderURL: import.meta.env.VITE_ORDER_URL,
}
