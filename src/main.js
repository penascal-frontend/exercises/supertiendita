import "@/styles/style.css"
import "@/core/stores"

import { loadProducts, addSelectedToBasket} from '@/app/selector'
import { cleanBasket } from "./app/basket"


document.addEventListener('DOMContentLoaded', event => {
  document.getElementById('addProductButton')
    .addEventListener('click', addSelectedToBasket)

  cleanBasket()
  loadProducts()
})
