import { JSDOM } from "jsdom"
import { describe, expect, vi, it } from "vitest"
import { loadProducts } from "@/app/selector"
import { cleanBasket } from "@/app/basket"


beforeAll(() =>{
  const dom = new JSDOM()
  global.document = dom.window.document
  global.window = dom.window

  global.fetch = vi.fn()

})


describe("User Story 010 - Iniciar la aplicación", () => {
  


  it("Clean basket", async () => {
    document.body.innerHTML = '<section id="basket">INTITAL VALUES</section>'

    await cleanBasket()

    expect(document.getElementById("basket").innerHTML).toStrictEqual('')
  })


  it("Load Products", async () => {
    const products = [
      { id: "ded3700478b849a3800d4ea8532f1b0b", name: "Tomate", price: 2.5, stock: 100 },
      { id: "e32d0f620b144564a46a324ab0b4ad04", name: "Zanahoria", price: 1.75, stock: 80 },
      { id: "fa85dd1c76bf431b96a95a8e177e13fa", name: "Cebolla", price: 1.25, stock: 120 },
    ]

    const options = [
      '<option value="fa85dd1c76bf431b96a95a8e177e13fa">Cebolla - 1.25€/Kg</option>',
      '<option value="ded3700478b849a3800d4ea8532f1b0b">Tomate - 2.50€/Kg</option>',
      '<option value="e32d0f620b144564a46a324ab0b4ad04">Zanahoria - 1.75€/Kg</option>',
    ].join("")

    document.body.innerHTML = '<select id="selector"></select>'

    global.fetch = vi.fn()
    fetch.mockResolvedValue({ json: () => products })

    await loadProducts()

    expect(document.getElementById("selector").innerHTML).toStrictEqual(options)
  })

})
