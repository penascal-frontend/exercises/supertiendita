import { JSDOM } from 'jsdom'
import { describe, expect, vi, it, beforeAll, beforeEach } from 'vitest'
import '@/app/basket'
import { addSelectedToBasket } from '@/app/selector'
import { productStore, basketStore } from '@/core/stores'

function getSelectedProductId() {
    const selector = document.getElementById('selector')
    return selector.value
}

function getIds(container_id, child_tagname, property) {
    const container = document.getElementById(container_id)
    const childNodesArray = Array.from(container.childNodes)
    const ids = childNodesArray
        .filter((child) => child.tagName === child_tagname)
        .map((elem) => elem[property])
    return ids
}

function getIdsInSelector() {
    return getIds('selector', 'OPTION', 'value')
}

function getIdsInBasket() {
    return getIds('basket', 'ARTICLE', 'id')
}

beforeAll(async () => {
    const dom = new JSDOM()
    global.document = dom.window.document
    global.window = dom.window

    global.fetch = vi.fn()
    fetch.mockResolvedValue({
        json: () => ({
            id: 'fa85dd1c76bf431b96a95a8e177e13fa',
            name: 'Cebolla',
            price: 1.25,
            stock: 120
        })
    })
})

beforeEach(async () => {
    document.body.innerHTML = `
      <select id="selector"></select>
      <section id="basket"></section>
    `

    productStore.value = [
        { id: "e32d0f620b144564a46a324ab0b4ad04", name: "Zanahoria", price: 1.75, stock: 80 },
        { id: "fa85dd1c76bf431b96a95a8e177e13fa", name: "Cebolla", price: 1.25, stock: 120 },
    ]

    basketStore.value = [
        { id: "ded3700478b849a3800d4ea8532f1b0b", name: "Tomate", price: 2.5, stock: 100, quantity:10 },
    ]
    document.getElementById('selector').value = 'fa85dd1c76bf431b96a95a8e177e13fa'
})

describe('User Story 020 - Agregar a la cesta', () => {
    it('Que aparezca el producto en la cesta', async () => {
        const id = getSelectedProductId()

        await addSelectedToBasket()

        const ids = getIdsInBasket()
        expect(ids).toContain(id)
    })

    it('Que desaparezca el producto del selector de productos', async () => {
        const id = getSelectedProductId()

        await addSelectedToBasket()

        const ids = getIdsInSelector()
        expect(ids).not.toContain(id)
    })
})
